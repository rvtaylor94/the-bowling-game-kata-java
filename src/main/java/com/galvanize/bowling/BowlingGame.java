package com.galvanize.bowling;

public class BowlingGame {
    private final int[] rolls = new int[21];
    private int score;


    public void roll(int pins, int index) {
        rolls[index] = pins;
    }

    public int getScore() {
        int currentBall = 0;

        for (int i = 0; i < 10; i++) {
            if (isStrike(currentBall)) {
                score += handleBonus(currentBall);
                currentBall++;
            } else if (isSpare(currentBall)) {
                score += handleBonus(currentBall);
                currentBall += 2;
            } else {
                score += scoreCurrentFrame(currentBall);
                currentBall += 2;
            }
        }
        return score;
    }

    private boolean isSpare(int index) {
        return scoreCurrentFrame(index) == 10;
    }

    private boolean isStrike(int index) {
        return rolls[index] == 10;
    }

    private int handleBonus(int index) {
        return rolls[index] + rolls[index + 1] + rolls[index + 2];
    }

    private int scoreCurrentFrame(int index) {
        return rolls[index] + rolls[index + 1];
    }
}
