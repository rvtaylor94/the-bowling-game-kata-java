package com.galvanize.bowling;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class BowlingGameTest {
    BowlingGame gameTest;

    @BeforeEach
    void setUp() {
        gameTest = new BowlingGame();
    }

    @Test
    void canCreateNewGame() {
        BowlingGame newGame = new BowlingGame();
        assertEquals(0, newGame.getScore());
    }

    @Test
    void canRollBall() {
        gameTest.roll(5, 0);
        assertEquals(5, gameTest.getScore());
    }

    @Test
    void canScoreAllGutters() {
        for (int i = 0; i < 20; i++) {
            gameTest.roll(0, i);
        }

        assertEquals(0, gameTest.getScore());
    }

    @Test
    void canScoreAllOnes() {
        for (int i = 0; i < 20; i++) {
            gameTest.roll(1, i);
        }

        assertEquals(20, gameTest.getScore());
    }

    @Test
    void canScoreSpare() {
        gameTest.roll(5, 0);
        gameTest.roll(5, 1);
        gameTest.roll(3, 2);
        for (int i = 3; i < 20; i++) {
            gameTest.roll(0, i);
        }

        assertEquals(16, gameTest.getScore());
    }

    @Test
    void canScoreStrike() {
        gameTest.roll(10, 0);
        gameTest.roll(3, 1);
        gameTest.roll(3, 2);
        for (int i = 3; i < 20; i++) {
            gameTest.roll(0, i);
        }

        assertEquals(22, gameTest.getScore());
    }
    
    @Test
    void canScorePerfectGame() {
        for (int i = 0; i < 12; i++) {
            gameTest.roll(10, i);
        }

        assertEquals(300, gameTest.getScore());
    }
}